import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipie-list',
  templateUrl: './recipie-list.component.html',
  styleUrls: ['./recipie-list.component.css'],
})
export class RecipieListComponent implements OnInit {
  @Output() recipeWasClicked = new EventEmitter<Recipe>();
  recipes: Recipe[] = [
    new Recipe(
      'A test Recipe',
      'This is a test description',
      'https://images.immediate.co.uk/production/volatile/sites/30/2020/08/chorizo-mozarella-gnocchi-bake-cropped-9ab73a3.jpg?webp=true&quality=90&resize=554%2C503'
    ),
    new Recipe(
      'Second Recipe',
      'Second Description',
      'https://www.tasteofhome.com/wp-content/uploads/2020/01/Easy-Pad-Thai_EXPS_FT20_249632_F_0109_1.jpg?resize=768,768'
    ),
    new Recipe(
      'Third Recipe',
      'Third Description',
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQbpIMoTCkQ7S-uT03zXEvFuY4Wd_BLafoUWw&usqp=CAU'
    ),
  ];
  constructor() {}

  ngOnInit(): void {}
  onRecipeSelected(recipeEl: Recipe) {
    this.recipeWasClicked.emit(recipeEl);
  }
}
